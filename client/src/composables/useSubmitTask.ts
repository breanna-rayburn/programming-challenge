import api from '../utils/axios';

import type { Task } from '../utils/types';

export const useSubmitTask = (inputContent: Task["content"], updateTasks: Function) => {
  /**
   * Mission two: Insert a task into the database.
   * 
   * Write and return a function here which will submit
   * a JSON object to the server to be inserted into the
   * database. Make sure that the response
   * from the server is then placed into the tasks list.
   * 
   * Definition of done:
   * [ ] the function sends a post request to the server
   * [ ] the server inserts the task into the database
   * [ ] the newly inserted task is placed into the tasks list
   * 
   * Your submission will be judged out of 10 points based on
   * the following criteria:
   * 
   * - Works as expected - 5 points
   * - Code quality - 5 points
   *   - Is the code clean and easy to read?
   *   - Are there any obvious bugs?
   *   - Are there any obvious performance issues?
   *   - Are there comments where necessary?
   */

  // Construct submit function to post to api and pass input value
  const submitTasks = () => {
    api.post('/submit-task', inputContent)
    .then((response) => {
      console.log(response);
      // Call getTasks to update the current task list after the response is complete
      // We need call in this function to ensure the database has been updated
      updateTasks();
    })
    .catch((error) => {
      console.error(error);
    });
  }

  submitTasks();

  return;
};
